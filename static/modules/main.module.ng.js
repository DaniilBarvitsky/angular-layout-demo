'use strict';

(function(angular){
    var module = angular.module('dsui-portal',
    [
        'dsui-navigation',
        'dsui-layouts',
        'ds-demo'
    ]);

    module.controller('main',['$scope',function($scope){
    }]);
})(/* global */ angular);