'use static';
(function(){
    var module = angular.module('dsui-controls',['ngMaterial','ngAnimate','ds-utilities']);

    module.directive('dsuiRibbonGroup',['transclusionBridge','$compile',function(transclusionBridge,$compile){
        return {
            restrict: 'E',
            scope: false,
            transclude: false,
            link: function(scope,element,attrs){
                var section = attrs['ribbonSectionId'];
                var group = attrs['ribbonGroupId'];
                element.detach();
                var content = element;
                transclusionBridge.requestHost('ribbon')
                    .then(function(ribbon){
                        ribbon.appendGroup(section,group);
                        scope.$on("$destroy",function(){
                            ribbon.removeGroup(section,group);
                            ribbon.removeSlot(group);
                        });
                        return ribbon.requestSlot(group);
                    })
                    .then(function(slot){
                        slot.append(element);
                    });                
            }
        }
    }]);

    module.directive('ribbonGroupContent',['transclusionBridge',function(transclusionBridge){
        return {
            restrict: 'E',
            scope: {
                group: '<ribbonGroupId'
            },
            transclude: true,
            controller: ['$scope','$element',function($scope,$element){
                transclusionBridge.requestHost('ribbon')
                    .then(function(ribbon){
                        return ribbon.provideSlot($scope.group,$element); 
                    })
            }],
            template: '<ng-transclude/>'
        }
    }]);

    module.directive('dsuiRibbon',['transclusionBridge',function(transclusionBridge){
        return {
            restrict: 'E',
            scope: {
            },
            controllerAs: '$ctrl',
            controller: ['$scope',function($scope){
                $scope.sections = [
                ];
                $scope.selected = null;

                transclusionBridge.provideHost('ribbon',{
                    appendGroup: function( sectionId, groupId ) {
                        var section = _.find($scope.sections, function(sec) { return sec.title == sectionId; });
                        if (section == null) {
                            section = { title: sectionId, groups: [] };
                            $scope.sections.push( section );
                        }
                        section.groups.push({
                                title: groupId
                        });
                        if (!$scope.selected) $scope.selected = section;
                    },
                    removeGroup: function( sectionId, groupId) {
                        var section = _.find($scope.sections, function(sec) { return sec.title == sectionId; });
                        if (section != null) {
                            _.remove(section.groups,function(group) { return group.title == groupId;});
                        }
                    }
                });
                
                this.selectSection = function( section ) {
                    $scope.selected = section;
                }
            }],
            templateUrl: '/modules/controls/ribbon.ng.html'
        }
    }]);
})(/*global*/angular);