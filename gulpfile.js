const gulp = require('gulp');
const inject = require('gulp-inject');

gulp.task('default',function(){

});


 
gulp.task('index', function () {
  var target = gulp.src('./static/index.html');
  // It's not necessary to read the files (will speed up things), we're only after their paths: 
  var sources = gulp.src(
      [
        './static/**/*.js', 
        './static/**/*.css'
      ], {read: false});
 
  return target.pipe(inject(sources,{relative:true}))
    .pipe(gulp.dest('./.temp'));
});