'use strict';
(function(angular){
    var module = angular.module('ds-demo',
    [
        'dsui-controls',
        'ngMaterial',
        'ui.router'
    ]);

    module.controller('demoController1',['$scope','$element',function($scope,$element){
        this.test = function(){
            console.log('Clicked');
            $scope.enabled = !$scope.enabled;
        }
    }]);

    module.controller('demoController2',['$scope','$element',function($scope,$element){
    }]);

    module.controller('demoController2action1',['$scope','$element',function($scope,$element){
    }]);

    module.config(['$stateProvider','stateToolsProvider',function($stateProvider,stateToolsProvider){
        $stateProvider.state({name: 'app',isVirtual: true})
        .state('app.state1',{
            url: '/one',
            views: {
                'content@': { controller: 'demoController1 as $ctrl', templateUrl: '/modules/demo/demo1.ng.html' },
            },
            data : {
                layout: { }
            }
        })
        .state('app.state2',{
            url: '/two',
            views: {
                'content@': { controller: 'demoController2', templateUrl: '/modules/demo/demo2.ng.html' },
                'bottom@': { template:'Video timeline here'}
            },
            data : {
                latyout: {}
            }
        });

        /* Example of building more complex states: */

        stateToolsProvider.modal('app.state2.action1', 
            {
                templateUrl:'/modules/demo/demo2.action1.ng.html', 
                controller: 'demoController2action1'
            }).register();
        
        stateToolsProvider.wizard('app.state2.wizard')
            .step('page1',{ templateUrl: '/modules/demo/demo2.wizard.page1.html'})
            .step('page2',{ templateUrl: '/modules/demo/demo2.wizard.page2.html'})
            .step('page3',{ templateUrl: '/modules/demo/demo2.wizard.page3.html'})
            .register();            
    }]);    
})(/* global */ angular);