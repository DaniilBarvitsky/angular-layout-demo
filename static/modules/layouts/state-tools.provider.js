'use strict';
(function(angular){
    var module = angular.module('dsui-layouts');

    module.provider('stateTools',['$stateProvider',function($stateProvider){

        function StateBuilder(name,params) {
            this.name = name;
            this.definition = {};
        }
        StateBuilder.prototype.override = function(config) { Object.assign(this.definition,config); return this; };
        StateBuilder.prototype.register = function() { $stateProvider.state(this.name,this.definition); return this; }
        StateBuilder.prototype.makeModal = function() {
            this.definition.onEnter = ['$state','$mdDialog','$rootScope',function($state,$mdDialog,$rootScope){
                var scope = $rootScope.$new(true);
                scope.$modalCancel = function() { $mdDialog.cancel();}
                scope.$modalOK = function(arg) { $mdDialog.hide(arg);}
                $mdDialog.show({
                    hasBackdrop : true,
                    template:'<ui-view name="modal">&nbsp;</ui-view>',
                    autoWrap: true,
                    scope: scope
                }).then(function(result){
                    if (result && result.navigate) $state.go(result.navigate);
                    else $state.go('^');
                }).catch(function(error){
                    console.log('Dialog error',error);
                    $state.go('^');
                });
            }];
            return this;
        }

        Object.defineProperty( StateBuilder.prototype, 'config', {
            get : function() { return this.definition; }
        });
        

        function WizardBuilder(name) {
            StateBuilder.call(this,name);
            this.definition.isVirtual = true;
            this.makeModal();
        }        

        WizardBuilder.prototype = Object.create(StateBuilder.prototype,{
            step : { value: function( name, config ) {
                new StateBuilder(this.name+"."+name).override({views: { 'modal@': config }}).register();
                return this;
            }}
        });

        WizardBuilder.prototype.constructor = WizardBuilder;

        this.state = function(name) {
            return new StateBuilder(name);
        };

        this.modal = function(name,params) {
            return new StateBuilder(name).override({ views: { 'modal@': params } }).makeModal();
        };
        
        this.wizard = function(name,params) {
            return new WizardBuilder(name);
        };

        this.$get = StateBuilder;
    }]);
})(angular);