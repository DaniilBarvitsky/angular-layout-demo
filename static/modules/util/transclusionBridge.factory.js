'use strict';
(function(angular){
    var module = angular.module('ds-utilities',[]);
    /* Utility class for promise-based dependency resolution */
    function Registry($q) {
        var instances = {};
        this.requestInstance = function(id) {
            return $q( function(resolve,reject){
                var instance = instances[id];
                if (!instance) {
                    instance = { link_queue: []};
                    instances[id] = instance;
                }
                if (instance.value) resolve(instance.value);
                else instance.link_queue.push(resolve);
            });
        };
        this.provideInstance = function(id,value) {
            var instance = instances[id];
            if (!instance) {
                instance = { value: value };
                instances[id] = instance;
            } else {
                instance.value = value;
            }
            if (instance.link_queue) _.forEach(instance.link_queue,function(waiter) { waiter(instance.value);});
            instance.link_queue = [];
        };
        this.removeInstance = function(id) {
            var instance = instances[id];
            if (instance && instance.value) delete instances[id];
        }
    }

    module.service('transclusionBridge',['$q',function($q){
        var hostRegistry = new Registry($q);
        return {
            provideHost: function( name, host ) {
                var slotRegistry = new Registry($q);
                host.provideSlot = function(id,element) { slotRegistry.provideInstance(id,element);};
                host.requestSlot = function(id) { return slotRegistry.requestInstance(id); };
                host.removeSlot = function(id) { slotRegistry.removeInstance(id); };
                hostRegistry.provideInstance(name, host);
            },
            requestHost: function( name ) {
                return hostRegistry.requestInstance(name);
            }
        }
    }]);
})(/* global */ angular);