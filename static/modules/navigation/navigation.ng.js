'use strict';
(function(angular){
    var module = angular.module('dsui-navigation',
    [
        'ngMaterial',
        'ui.router'
    ]);

    module.directive('dsuiTopBar',function(){
        return {
            restrict: 'E',
            templateUrl: '/modules/navigation/topbar.ng.html'
        };
    });
})(/* global */ angular);