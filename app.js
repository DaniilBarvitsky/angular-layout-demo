'use strict';

const koa = require('koa');
const app = koa();
const koa_ = {
    router : require('koa-router'),
    logger : require('koa-logger'),
    serve : require('koa-static'),
    port : process.env.PORT || 8080
}
app.use(koa_.logger());
app.use(koa_.serve('./bower_components'));
app.use(koa_.serve('./.temp'));
app.use(koa_.serve('./static'));

app.listen(koa_.port);

console.log('Listening on', koa_.port );
