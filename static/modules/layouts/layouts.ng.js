'use strict';
(function(angular){
    var module = angular.module('dsui-layouts',
    [
        'ngMaterial',
        'ngAnimate',
        'ui.router',
        'dsui-controls'
    ]);

    function LayoutBox(defaults) {
        Object.assign(this,defaults);
    }

    Object.defineProperty(LayoutBox.prototype,'style',{
        get: function() {
            var style = {};
            var self = this;
            _.each(['left','top','right','bottom','width','height'],function(prop){
                if (self.hasOwnProperty(prop)) style[prop] = self[prop]+'px';
            });
            return style;
        }
    });

    module.directive('dsuiDefaultLayout',function(){
        return {
            restrict: 'E',
            controllerAs: '$main',
            controller: ['$scope','$mdSidenav','$state','$mdDialog','$element',function($scope,$mdSidenav,$state,$mdDialog,$element) {
                $scope.layout = {
                    'ribbon': new LayoutBox({ isOpen: true, left:0, top: 0, right: 0, height: 96}),
                    'top' : new LayoutBox({ isOpen: true, left:0, top:0, right: 0, height: 48}),
                    'main' : new LayoutBox({ left: 0, top: 0, right: 0, bottom: 0 }),
                    'outline' : new LayoutBox({ isOpen: true, left:0, top: 0, bottom: 0, width: 200 }),
                    'side' : new LayoutBox({isOpen: true, right:0, top:0, bottom: 0, width: 200 }),
                    'bottom' : new LayoutBox({isOpen: true, left:0, right:0, bottom:0, height: 200 }),
                    updateLayout: function () {
                        var cue_top_y = this.ribbon.isOpen ? this.ribbon.height : 0;
                        var cue_top_left = {
                            left: (this.outline.isOpen ? this.outline.width : 0),
                            top: (this.top.isOpen ? this.top.height : 0) + cue_top_y
                        };
                        var cue_bottom_right = {
                            right: (this.side.isOpen? this.side.width : 0 ),
                            bottom: (this.bottom.isOpen ? this.bottom.height : 0)
                        };

                        this.top.top = cue_top_y;
                        this.main.left = cue_top_left.left;
                        this.main.top = cue_top_left.top;
                        this.main.right = cue_bottom_right.right;
                        this.main.bottom = cue_bottom_right.bottom;
                        this.outline.top = cue_top_left.top; 
                        this.side.top = cue_top_left.top;
                        this.side.bottom = cue_bottom_right.bottom;
                        this.bottom.left = cue_top_left.left;
                    }
                };                

                $scope.$watchGroup(['layout.outline.isOpen','layout.side.isOpen','layout.bottom.isOpen','layout.top.isOpen'],function(){
                    $scope.layout.updateLayout();
                });

                this.toggleMenu = function() {
                    $mdSidenav('left_main').toggle();
                };
            }],
            templateUrl: '/modules/layouts/default-layout.ng.html'
        };
    });
})(/* global */ angular);